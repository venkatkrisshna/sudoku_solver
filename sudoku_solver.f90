! ============= !
! SUDOKU SOLVER !
! ============= !
! Venkata Krisshna

! Ref: Basic solve,  https://www.youtube.com/watch?v=b123EURtu3I&t=8s
!      Backtracking, 

program sudoku_solver

  implicit none
  integer :: board(9,9),sol(9,9),maxsol(9,9)
  logical :: chance(9,9,9)=.true.
  integer :: boardsol(9,9)
  integer :: sublist(3,3,4,9)
  logical :: cheat=.false.
  logical :: fail=.false.
  logical :: fin=.false.
  integer :: basic_niter=0,backt_niter=0
  
  print*,'=================================='
  print*,'Welcome to Venkat''s Sudoku Solver!'
  print*,'=================================='
  call init
  call solve

contains

  ! =========================================== !
  !  Bactracking algorithm for advanced boards  !
  ! =========================================== !
  subroutine backtracking
    implicit none
    integer :: nunsol
    integer :: myi,myj
    integer :: i,j,nn,q
    integer :: guess(81,1:3),nguess
    character(len=3) :: failcode

    print*,'The Sudoku Board is challenging! Using backtracking algorithm to solve this board!'
    guess=0
    nguess=0
    fail=.false.
    backt_niter=1

    ! Calculate maximum possible solution for each cell
    maxsol=0
    do i=1,9
       do j=1,9
          qsol: if (sol(i,j).eq.0) then
             do q=9,1,-1
                if (chance(q,i,j)) then
                   maxsol(i,j)=q
                   exit qsol
                end if
             end do
          end if qsol
       end do
    end do
    
    ! Backtracking recursive loop
    backtrack: do
       
       if (fail) then
          ! If failed, go to the last guess
          if (guess(nguess,3).eq.maxsol(myi,myj)) then
             ! Reset current cell to unsolved, 0
             sol(myi,myj)=0
             ! Backtrack to penultimate cell
             penultimate: do nn=nguess-1,1,-1
                ! Find penultimate guessed cell
                if (myi.ne.guess(nn,1) .or. myj.ne.guess(nn,2)) then
                   if (guess(nn,3).eq.maxsol(guess(nn,1),guess(nn,2))) then
                      ! if maxed out here too, reset to unsolved, 0 and backtrack more
                      sol(guess(nn,1),guess(nn,2))=0
                   else
                      ! If not maxed out here
                      myi=guess(nn,1)  ! Set new cell position for guess
                      myj=guess(nn,2)  ! Set new cell position for guess
                      nguess=nn-1      ! Set new guess counter
                      guess(nn:81,:)=0 ! Clear guesses down from here
                      exit penultimate
                   end if
                end if
             end do penultimate
          else
             ! Go to previous cell
             myi=guess(nguess,1)  ! Set new cell position for guess
             myj=guess(nguess,2)  ! Set new cell position for guess
             guess(nguess:81,:)=0 ! Clear guesses down from here
             nguess=nguess-1      ! Set new guess counter
          end if

       else
          ! If not failed, find next unsolved cell
          board: do i=1,9
             do j=1,9
                if (sol(i,j).eq.0) then
                   myi=i
                   myj=j
                   exit board
                end if
             end do
          end do board
       end if

       ! Make a guess in this cell
       qguess: do q=sol(myi,myj)+1,9
          if (chance(q,myi,myj)) then
             sol(myi,myj)=q
             exit qguess
          end if
       end do qguess
       nguess=nguess+1
       guess(nguess,:)=[myi,myj,sol(myi,myj)]
       
       ! Validate board
       fail=.false.   ! Reset failsafe
       failcode='---' ! Reset failcode
       call backtrack_patrol(myi,myj,fail,failcode)
       call unsolved(nunsol)
       if (.not.fail .and. nunsol.eq.0) then
          fin=.true.
          exit backtrack
       end if
       if (nguess.gt.81**2) stop
       backt_niter=backt_niter+1
       ! print*,backt_niter,'I guessed',sol(myi,myj),'at',myi,myj,'and',fail,failcode,nunsol,maxsol(myi,myj)
       
    end do backtrack
    
    return
  end subroutine backtracking

  ! ============ !
  !  Read board  !
  ! ============ !
  subroutine board_init
    implicit none
    
    cheat=.true.
    open(21, file="board5")
    read(21,*) board
    if (cheat) then
       open(21, file="board5sol")
       read(21,*) boardsol
    end if
    sol=board
    ! call print_board
    
    return
  end subroutine board_init
  
  ! ==================== !
  !  Solver subroutines  !
  ! ==================== !
  subroutine solve
    implicit none
    integer :: unsol,unsolp
    integer :: i,j
    
    call unsolved(unsolp)
    print*,'Initial unsolved cells  ',unsolp,'/81 | Progress =',100-100*unsolp/81,'%'
    print*,'------------------- ! Initiating Sudoku Solver ! -------------------'
    
    call basic_solve
    if (fin) then
    else
       call backtracking
    end if
    unsolp=unsol
    
    ! Printing solution
    if (.not.fin) then
       call unsolved(unsol)
       print*,'Number of unsolved cells',unsol,'/81 | Progress =',100-100*unsol/81,'%'
       print*,'Current Solution:'
    else
       print*,'The Sudoku Board has been SOLVED!'
       print*,'Stats |        Basic solver iterations : ',basic_niter
       print*,'Stats | Backtracking solver iterations : ',backt_niter
       ! Check with solution
       if (cheat) then
          board: do i=1,9
             do j=1,9
                if(sol(i,j).ne.boardsol(i,j)) then
                   print*,'Boardsol: SOLVER HAS FAILED HERE!',i,j
                   fail=.true.
                else
                   print*,'Boardsol: SOLVER HAS SUCCEEDED!'
                end if
                exit board
             end do
          end do board
       end if
    end if
    print*,'Solution:'
    call print_board

    return
  end subroutine solve
  
  ! ============= !
  !  Basic solve  !
  ! ============= !
  subroutine basic_solve
    implicit none
    integer :: unsolp,unsol
    
    basic_niter=0
    unsolp=0
    do
       basic_niter=basic_niter + 1
       call naked_singles
       call hidden_singles
       call naked_pairs
       call pointing_pairs
       call claiming_pairs
       call naked_triples ! Pending development
       call x_wings
       call hidden_pairs
       call naked_quads ! Pending development
       call patrol
       ! Progress check
       call unsolved(unsol)
       if(unsolp.eq.unsol) then
          exit
       elseif (unsol.eq.0) then
          fin=.true.
          exit
       end if
       unsolp=unsol
    end do
    
    return
  end subroutine basic_solve

  ! ============== !
  !  Hidden Pairs  !
  ! ============== !
  subroutine hidden_pairs
    implicit none
    integer :: i,j,k,q,ii,jj,kk,qq,pos(2,2),q1,q2,ip,jp,pos2(2,2)
    
    ! Row Elimination
    do j=1,9
       do q=1,9
          q1=0
          q2=0
          pos=0
          if (count(chance(q,:,j)).eq.2) then
             q1=q
             k=0
             do i=1,9
                if (chance(q,i,j)) then
                   k=k+1
                   pos(1,k)=i
                end if
             end do
             do qq=1,9
                if (qq.eq.q) cycle
                if (count(chance(qq,:,j)).eq.2) then
                   q2=qq
                   kk=0
                   do ii=1,9
                      if (chance(qq,ii,j)) then
                         kk=kk+1
                         pos(2,kk)=ii
                      end if
                   end do
                end if
             end do
             if (pos(1,1).eq.pos(2,1) .and. pos(1,2).eq.pos(2,2)) then
                if (count(chance(:,pos(1,1),j)).gt.2 .or. count(chance(:,pos(1,2),j)).gt.2) then
                   ! Perform elimination
                   chance(: ,pos(1,1),j)=.false.
                   chance(: ,pos(1,2),j)=.false.
                   chance(q1,pos(1,1),j)=.true.
                   chance(q1,pos(1,2),j)=.true.
                   chance(q2,pos(2,1),j)=.true.
                   chance(q2,pos(2,2),j)=.true.
                end if
             end if
          end if
       end do
    end do
    
    ! Column Elimination
    do i=1,9
       do q=1,9
          q1=0
          q2=0
          pos=0
          if (count(chance(q,i,:)).eq.2) then
             q1=q
             k=0
             do j=1,9
                if (chance(q,i,j)) then
                   k=k+1
                   pos(1,k)=j
                end if
             end do
             do qq=1,9
                if (qq.eq.q) cycle
                if (count(chance(qq,i,:)).eq.2) then
                   q2=qq
                   kk=0
                   do jj=1,9
                      if (chance(qq,i,jj)) then
                         kk=kk+1
                         pos(2,kk)=jj
                      end if
                   end do
                end if
             end do
             if (pos(1,1).eq.pos(2,1) .and. pos(1,2).eq.pos(2,2)) then
                if (count(chance(:,i,pos(1,1))).gt.2 .or. count(chance(:,i,pos(1,2))).gt.2) then
                   ! Perform elimination
                   chance(: ,i,pos(1,1))=.false.
                   chance(: ,i,pos(1,2))=.false.
                   chance(q1,i,pos(1,1))=.true.
                   chance(q1,i,pos(1,2))=.true.
                   chance(q2,i,pos(2,1))=.true.
                   chance(q2,i,pos(2,2))=.true.
                end if
             end if
          end if
       end do
    end do

    ! Subgrid Elimination
    do jj=1,7,3
       do ii=1,7,3
          do q=1,9
             q1=0
             q2=0
             pos=0
             if (count(chance(q,ii:ii+2,jj:jj+2)).eq.2) then
                q1=q
                k=0
                do j=jj,jj+2
                   do i=ii,ii+2
                      if (chance(q,i,j)) then
                         k=k+1
                         pos(:,k)=[i,j]
                      end if
                   end do
                end do
                do qq=1,9
                   if (qq.eq.q) cycle
                   if (count(chance(qq,ii:ii+2,jj:jj+2)).eq.2) then
                      q2=qq
                      kk=0
                      do jp=jj,jj+2
                         do ip=ii,ii+2
                            if (chance(qq,ip,jp)) then
                               kk=kk+1
                               pos2(:,kk)=[ip,jp]
                            end if
                         end do
                      end do
                   end if
                end do
                if (all(pos(:,1).eq.pos2(:,1)) .and. all(pos(:,2).eq.pos2(:,2))) then
                   if (count(chance(:,pos(1,1),pos(2,1))).gt.2 .or. count(chance(:,pos(1,2),pos(2,2))).gt.2) then
                      ! Perform elimination
                      chance(: ,pos(1,1),pos(2,1))=.false.
                      chance(: ,pos(1,2),pos(2,2))=.false.
                      chance(q1,pos(1,1),pos(2,1))=.true.
                      chance(q1,pos(1,2),pos(2,2))=.true.
                      chance(q2,pos(1,1),pos(2,1))=.true.
                      chance(q2,pos(1,2),pos(2,2))=.true.
                   end if
                end if
             end if
          end do
       end do
    end do

    return
  end subroutine hidden_pairs

  ! ========= !
  !  X Wings  !
  ! ========= !
  subroutine x_wings
    implicit none
    integer :: i,j,ip,jp,q,pos(2,2),k
    logical :: wing

    ! Row
    do q=1,9
       do j=1,9
          wing=.false.
          if (count(chance(q,:,j)).eq.2) then
             k=0
             do i=1,9
                if (chance(q,i,j)) then
                   k=k+1
                   pos(1,k)=i
                end if
             end do
             pos(2,1)=j
             rowloop: do jp=1,9
                if (j.eq.jp) cycle
                if (chance(q,pos(1,1),jp)) then
                   if (count(chance(q,:,jp)).eq.2 .and. chance(q,pos(1,2),jp)) then
                      pos(2,2)=jp
                      wing=.true. ! X-Wing found!
                      exit rowloop
                   end if
                end if
             end do rowloop
          end if
          if (wing) then
             ! Perform elimination
             chance(q,pos(1,1),:)=.false.
             chance(q,pos(1,2),:)=.false.
             chance(q,pos(1,1),pos(2,1))=.true.
             chance(q,pos(1,1),pos(2,2))=.true.
             chance(q,pos(1,2),pos(2,1))=.true.
             chance(q,pos(1,2),pos(2,2))=.true.
          end if
       end do
    end do

    ! Column
    do q=1,9
       do i=1,9
          wing=.false.
          if (count(chance(q,i,:)).eq.2) then
             k=0
             do j=1,9
                if (chance(q,i,j)) then
                   k=k+1
                   pos(2,k)=j
                end if
             end do
             pos(1,1)=i
             colloop: do ip=1,9
                if (i.eq.ip) cycle
                if (chance(q,ip,pos(2,1))) then
                   if (count(chance(q,ip,:)).eq.2 .and. chance(q,ip,pos(2,2))) then
                      pos(1,2)=ip
                      wing=.true. ! X-Wing found!
                      exit colloop
                   end if
                end if
             end do colloop
          end if
          if (wing) then
             ! Perform elimination
             chance(q,:,pos(2,1))=.false.
             chance(q,:,pos(2,2))=.false.
             chance(q,pos(1,1),pos(2,1))=.true.
             chance(q,pos(1,1),pos(2,2))=.true.
             chance(q,pos(1,2),pos(2,1))=.true.
             chance(q,pos(1,2),pos(2,2))=.true.
          end if
       end do
    end do

    return
  end subroutine x_wings

  ! ================ !
  !  Claiming Pairs  !
  ! ================ !
  subroutine claiming_pairs
    implicit none
    integer :: i,j,q,count,ii(3),jj(3)
    logical :: point

    ! Row
    do q=1,9
       do j=1,9
          ii=0
          point=.true.
          row: do i=1,9
             if (chance(q,i,j)) then
                if (count.eq.0 .and. mod(i,3).eq.0) then
                   point=.false.
                   exit row
                end if
                if (count.eq.3) then
                   point=.false.
                   exit row
                end if
                count=count+1
                ii(count)=i
             end if
          end do row
          if (count.ge.2 .and. point) then
             ! ID if same subgrid
             if (mod(ii(1),3).eq.1) then
                if (ii(2).eq.ii(1)+1) then
                   if (ii(3).eq.0 .or. ii(3).eq.ii(1)+2) then
                      ! Perform elimination
                      if (mod(j,3).eq.0) then
                         chance(q,ii(1):ii(1)+2,j-2:j  )=.false.
                      elseif (mod(j,3).eq.1) then
                         chance(q,ii(1):ii(1)+2,j  :j+2)=.false.
                      elseif (mod(j,3).eq.2) then
                         chance(q,ii(1):ii(1)+2,j-1:j+1)=.false.
                      end if
                      chance(q,ii(1:count),j)=.true.
                   end if
                end if
             end if
             if (mod(ii(1),3).eq.2) then
                if (ii(2).eq.ii(1)+1 .and. ii(3).eq.0) then
                   ! Perform elimination
                   if (mod(j,3).eq.0) then
                      chance(q,ii(1):ii(1)+1,j-2:j)=.false.
                   elseif (mod(j,3).eq.1) then
                      chance(q,ii(1):ii(1)+1,j:j+2)=.false.
                   elseif (mod(j,3).eq.2) then
                      chance(q,ii(1):ii(1)+1,j-1:j+1)=.false.
                   end if
                   chance(q,ii(1:count),j)=.true.
                end if
             end if
          end if
       end do
    end do

    ! Col
    do q=1,9
       do i=1,9
          jj=0
          point=.true.
          col: do j=1,9
             if (chance(q,i,j)) then
                if (count.eq.0 .and. mod(j,3).eq.0) then
                   point=.false.
                   exit col
                end if
                if (count.eq.3) then
                   point=.false.
                   exit col
                end if
                count=count+1
                jj(count)=i
             end if
          end do col
          if (count.ge.2 .and. point) then
             ! ID if same subgrid
             if (mod(jj(1),3).eq.1) then
                if (jj(2).eq.jj(1)+1) then
                   if (jj(3).eq.0 .or. jj(3).eq.jj(1)+2) then
                      ! Perform elimination
                      if (mod(i,3).eq.0) then
                         chance(q,i-2:i  ,jj(1):jj(1)+2)=.false.
                      elseif (mod(i,3).eq.1) then
                         chance(q,i  :i+2,jj(1):jj(1)+2)=.false.
                      elseif (mod(i,3).eq.2) then
                         chance(q,i-1:i+1,jj(1):jj(1)+2)=.false.
                      end if
                      chance(q,i,jj(1:count))=.true.
                   end if
                end if
             end if
             if (mod(jj(1),3).eq.2) then
                if (jj(2).eq.jj(1)-1) then
                   if (jj(3).eq.0 .or. jj(3).eq.jj(1)+1) then
                      ! Perform elimination
                      if (mod(i,3).eq.0) then
                         chance(q,i-2:i  ,jj(1):jj(1)+1)=.false.
                      elseif (mod(i,3).eq.1) then
                         chance(q,i  :i+2,jj(1):jj(1)+1)=.false.
                      elseif (mod(i,3).eq.2) then
                         chance(q,i-1:i+1,jj(1):jj(1)+1)=.false.
                      end if
                      chance(q,i,jj(1:count))=.true.
                   end if
                end if
             end if
          end if
       end do
    end do

    return
  end subroutine claiming_pairs

  ! ================ !
  !  Pointing Pairs  !
  ! ================ !
  subroutine pointing_pairs
    implicit none
    integer :: i,j,ii,jj,q,subi,subj,count
    integer :: pos(2,3)
    logical :: point=.true.

    do subj=1,3
       do subi=1,3
          i=(subi-1)*3+1
          j=(subj-1)*3+1
          do q=1,9
             count=0
             pos  =0
             point=.true.
             subgrid: do jj=j,j+2
                do ii=i,i+2
                   if (chance(q,ii,jj)) then
                      if (count.eq.3) then
                         point=.false.
                         exit subgrid
                      end if
                      count=count+1
                      pos(:,count)=[ii,jj]
                   end if
                end do
             end do subgrid
             if (count.ge.2 .and. point) then
                ! ID if same row/column
                if (all(pos(1,1:count).eq.pos(1,1)) .or. all(pos(2,1:count).eq.pos(2,1))) then
                   if (all(pos(1,1:count).eq.pos(1,1))) then
                      ! Perform elimination
                      chance(q,pos(1,1),:)=.false.
                      chance(q,pos(1,1),pos(2,1:count))=.true.
                   end if
                   if (all(pos(2,1:count).eq.pos(2,1))) then
                      ! Perform elimination
                      chance(q,:,pos(2,1))=.false.
                      chance(q,pos(1,1:count),pos(2,1))=.true.
                   end if
                end if
             end if
          end do
       end do
    end do

    return
  end subroutine pointing_pairs

  ! =============== !
  !  Naked Singles  !
  ! =============== !
  subroutine naked_singles
    implicit none
    integer :: i,j,q

    do i=1,9
       do j=1,9
          if (count(chance(:,i,j)).eq.1 .and. sol(i,j).eq.0) then
             num: do q=1,9
                if (chance(q,i,j)) exit num
             end do num
             call update([i,j],q)
          end if
       end do
    end do

    return
  end subroutine naked_singles

  ! ================ !
  !  Hidden Singles  !
  ! ================ !
  subroutine hidden_singles
    implicit none
    integer :: i,j,q,ii,jj,count,pos(2)

    ! Hidden single - Column
    do q=1,9
       do i=1,9
          count=0
          col: do j=1,9
             if(sol(i,j).eq.0) then
                if (chance(q,i,j)) then
                   count=count + 1
                   jj=j
                end if
                if (count.gt.1) exit col
             end if
          end do col
          if (count.eq.1) call update([i,jj],q)
       end do
    end do

    ! Hidden single - Row
    do q=1,9
       do j=1,9
          count=0
          row: do i=1,9
             if(sol(i,j).eq.0) then
                if (chance(q,i,j)) then
                   count=count + 1
                   ii=i
                end if
                if (count.gt.1) exit row
             end if
          end do row
          if (count.eq.1) call update([ii,j],q)
       end do
    end do

    ! Hidden single - Subgrid
    do q=1,9
       do jj=1,7,3
          do ii=1,7,3
             count=0
             subgrid: do j=jj,jj+2
                do i=ii,ii+2
                   if(sol(i,j).eq.0) then
                      if (chance(q,i,j)) then
                         count=count + 1
                         pos=[i,j]
                      end if
                      if (count.gt.1) exit subgrid
                   end if
                end do
             end do subgrid
             if (count.eq.1) call update(pos,q)
          end do
       end do
    end do

    return
  end subroutine hidden_singles

  ! ============= !
  !  Naked pairs  !
  ! ============= !
  subroutine naked_pairs
    implicit none
    integer :: i,j,q,ii,jj,oi,oj,q1a,q1b,q2a,q2b,q1,q2
    integer :: r,rr
    integer :: subi,subj,i1,j1,i2,j2

    ! Twin pair row elimination
    do i=1,9
       oj=0
       do j=1,9
          if (j.eq.oj) cycle
          if (count(chance(:,i,j)).eq.2) then
             do jj=1,9
                if (count(chance(:,i,jj)).eq.2) then
                   if (j.ne.jj) then
                      oj=jj
                      do q=1,9
                         if (chance(q,i,j)) then
                            q1a=q
                            exit
                         end if
                      end do
                      do q=1,9
                         if (q.eq.q1a) cycle
                         if (chance(q,i,j)) then
                            q1b=q
                            exit
                         end if
                      end do
                      do q=1,9
                         if (chance(q,i,jj)) then
                            q2a=q
                            exit
                         end if
                      end do
                      do q=1,9
                         if (q.eq.q2a) cycle
                         if (chance(q,i,jj)) then
                            q2b=q
                            exit
                         end if
                      end do
                      if (q1a.eq.q2a .and. q1b.eq.q2b) then
                         ! Perform elimination
                         chance(q1a,i,: )=.false.
                         chance(q1b,i,: )=.false.
                         chance(q1a,i,j )=.true.
                         chance(q1b,i,j )=.true.
                         chance(q1a,i,jj)=.true.
                         chance(q1b,i,jj)=.true.
                      end if
                   end if
                end if
             end do
          end if
       end do
    end do

    ! Twin pair col elimination
    do j=1,9
       oi=0
       do i=1,9
          if (i.eq.oi) cycle
          if (count(chance(:,i,j)).eq.2) then
             do ii=1,9
                if (count(chance(:,ii,j)).eq.2) then
                   if (i.ne.ii) then
                      oi=ii
                      do q=1,9
                         if (chance(q,i,j)) then
                            q1a=q
                            exit
                         end if
                      end do
                      do q=1,9
                         if (q.eq.q1a) cycle
                         if (chance(q,i,j)) q1b=q
                      end do
                      do q=1,9
                         if (chance(q,ii,j)) then
                            q2a=q
                            exit
                         end if
                      end do
                      do q=1,9
                         if (q.eq.q2a) cycle
                         if (chance(q,ii,j)) q2b=q
                      end do
                      if (q1a.eq.q2a .and. q1b.eq.q2b) then
                         ! Perform elimination
                         chance(q1a,: ,j)=.false.
                         chance(q1b,: ,j)=.false.
                         chance(q1a,i ,j)=.true.
                         chance(q1b,i ,j)=.true.
                         chance(q1a,ii,j)=.true.
                         chance(q1b,ii,j)=.true.
                      end if
                   end if
                end if
             end do
          end if
       end do
    end do

    ! Twin pair subgrid elimination
    do subj=1,3
       do subi=1,3
          sublist=0
          call form_sublist(subi,subj)
          ! Check twin pair
          list1: do rr=1,9
             if (sublist(subi,subj,1,rr).eq.0) exit list1
             list2: do r=1,9
                if (sublist(subi,subj,1,r).eq.0) exit list2
                if (r.eq.rr) cycle
                if (sublist(subi,subj,3,r).eq.sublist(subi,subj,3,rr) .and.&
                     sublist(subi,subj,4,r).eq.sublist(subi,subj,4,rr)) then
                   i1=sublist(subi,subj,1,r)
                   j1=sublist(subi,subj,2,r)
                   i2=sublist(subi,subj,1,rr)
                   j2=sublist(subi,subj,2,rr)
                   q1=sublist(subi,subj,3,r)
                   q2=sublist(subi,subj,4,r)
                   ii=(subi-1)*3+1
                   jj=(subj-1)*3+1
                   ! Perform elimination
                   chance(q1,ii:ii+2,jj:jj+2)=.false.
                   chance(q2,ii:ii+2,jj:jj+2)=.false.
                   chance(q1,i1,j1)=.true.
                   chance(q2,i1,j1)=.true.
                   chance(q1,i2,j2)=.true.
                   chance(q2,i2,j2)=.true.
                end if
             end do list2
          end do list1
       end do
    end do

    return
  end subroutine naked_pairs

  ! ========================== !
  !  Initializing subroutines  !
  ! ========================== !
  subroutine init
    implicit none

    call board_init
    call chance_init

    return
  end subroutine init

  ! ============================ !
  !  Update solution and chance  !
  ! ============================ !
  subroutine update(pos,q)
    implicit none
    integer, intent(in) :: pos(2),q
    integer :: i,j

    ! Insert cell solution
    sol(pos(1),pos(2))=q

    if (cheat) then
       ! Check with solution
       if(sol(pos(1),pos(2)).ne.boardsol(pos(1),pos(2))) print*,'Boardsol: SOLVER HAS FAILED HERE!',pos
    end if

    ! Perform eliminations after new entry
    i=pos(1)+1-mod(pos(1),3)
    if (mod(pos(1),3).eq.0) i=i-3
    j=pos(2)+1-mod(pos(2),3)
    if (mod(pos(2),3).eq.0) j=j-3
    chance(:,pos(1),pos(2))=.false.      ! Occupied cell
    chance(q,:,pos(2))=.false.           ! Global col elimination
    chance(q,pos(1),:)=.false.           ! Global row elimination
    chance(q,i:i+2,j:j+2)=.false.        ! Subgrid elimination

    ! Check validity
    call patrol
    
    return
  end subroutine update

  ! ====================== !
  !  Count unsolved cells  !
  ! ====================== !
  subroutine unsolved(unsolv)
    implicit none
    integer :: i,j
    integer, intent(out) :: unsolv

    unsolv=0
    do j=1,9
       do i=1,9
          if(sol(i,j).eq.0) unsolv=unsolv + 1
       end do
    end do
    
    return
  end subroutine unsolved

  ! ============== !
  !  Form sublist  !
  ! ============== !
  subroutine form_sublist(subi,subj)
    implicit none
    integer, intent(in) :: subi,subj
    integer i,j,ii,jj,q,k

    i=(subi-1)*3+1
    j=(subj-1)*3+1
    k=0
    do jj=j,j+2
       do ii=i,i+2
          if (count(chance(:,ii,jj)).eq.2) then
             k=k+1
             sublist(subi,subj,1,k)=ii ! i coord
             sublist(subi,subj,2,k)=jj ! j coord
             ! ID q1 and q2
             do q=1,9
                if (chance(q,ii,jj)) then
                   sublist(subi,subj,3,k)=q ! q1
                   exit
                end if
             end do
             do q=1,9
                if (chance(q,ii,jj) .and. q.ne.sublist(subi,subj,3,k)) then
                   sublist(subi,subj,4,k)=q ! q2
                   exit
                end if
             end do
          end if
       end do
    end do

    return
  end subroutine form_sublist

  ! ============= !
  !  Print board  !
  ! ============= !
  subroutine print_board
    implicit none
    integer :: j

    do j=1,9
       print*,sol(:,j)
    end do

    return
  end subroutine print_board

  ! ================================= !
  !  Print Chance Board for a number  !
  ! ================================= !
  subroutine chance_board(q)
    implicit none
    integer, intent(in) :: q
    integer :: j

    print*,'Chance of',q
    do j=1,9
       print*,chance(q,:,j)
    end do

    return
  end subroutine chance_board

  ! ================= !
  !  Populate chance  !
  ! ================= !
  subroutine chance_init
    implicit none
    integer :: i,j,ii,jj

    do j=1,9
       do i=1,9
          if(board(i,j).ne.0) then
             ! Perform elimination
             chance(:,i,j)=.false.          ! Occupied cells
             chance(board(i,j),:,j)=.false. ! Global col elimination
             chance(board(i,j),i,:)=.false. ! Global row elimination
          end if
       end do
    end do

    ! Subgrid elimination
    do jj=1,7,3
       do ii=1,7,3
          do j=jj,jj+2
             do i=ii,ii+2
                ! Perform elimination
                if(board(i,j).ne.0) chance(board(i,j),ii:ii+2,jj:jj+2)=.false.
             end do
          end do
       end do
    end do

    return
  end subroutine chance_init

  ! =============== !
  !  Naked triples  !
  ! =============== !
  subroutine naked_triples
    implicit none

    ! Pending development

    return
  end subroutine naked_triples

  ! ============= !
  !  Naked quads  !
  ! ============= !
  subroutine naked_quads
    implicit none

    ! Pending development

    return
  end subroutine naked_quads
  
  ! ========================= !
  !  Patrol for failed cells  !
  ! ========================= !
  subroutine patrol
    implicit none
    integer :: i,j,si,sj,ii,jj

    do i=1,9
       do j=1,9
          ! No more chance
          if (sol(i,j).eq.0 .and. count(chance(:,i,j)).eq.0) fail=.true.
          ! Row
          if (any(sol(i,j).eq.sol(1:max(i-1,1),j)) .or. any(sol(i,j).eq.sol(min(9,i+1):9,j))) fail=.true.
          ! Column
          if (any(sol(i,j).eq.sol(i,1:max(j-1,1))) .or. any(sol(i,j).eq.sol(i,min(9,j+1):9))) fail=.true.
          ! Subgrid
          si=3*int((i-1)/3)+1
          sj=3*int((j-1)/3)+1
          do ii=si,si+2
             do jj=sj,sj+2
                if (ii.eq.i .and. jj.eq.j) cycle
                if (sol(i,j).eq.sol(ii,jj)) fail=.true.
             end do
          end do
       end do
    end do

    return
  end subroutine patrol
  
  ! ========================== !
  !  Patrol for guessed cells  !
  ! ========================== !
  subroutine backtrack_patrol(i,j,fail,failcode)
    implicit none
    integer, intent(in) :: i,j
    logical, intent(out) :: fail
    character(len=3), intent(out) :: failcode
    integer :: si,sj,ii,jj
    
    if (sol(i,j).eq.0) fail=.true.
    ! Row
    do ii=1,9
       if (i.eq.ii) cycle
       if (sol(i,j).eq.sol(ii,j)) then
          fail=.true.
          failcode='row'
          return
       end if
    end do
    ! Column
    do jj=1,9
       if (j.eq.jj) cycle
       if (sol(i,j).eq.sol(i,jj)) then
          fail=.true.
          failcode='col'
          return
       end if
    end do
    ! Subgrid
    si=3*int((i-1)/3)+1
    sj=3*int((j-1)/3)+1
    do ii=si,si+2
       do jj=sj,sj+2
          if (ii.eq.i .and. jj.eq.j) cycle
          if (sol(i,j).eq.sol(ii,jj)) then
             fail=.true.
             failcode='sub'
             return
          end if
       end do
    end do

    return
  end subroutine backtrack_patrol

end program sudoku_solver
